
public class Person implements Measurable, Comparable<Person> {

	private String name;
	private int hight;
	private int inc;
	
	public Person(String name,int hight,int inc){
		this.name = name;
		this.hight = hight;
		this.inc = inc;
	}
	
	public double getMeasure() {
		
		return hight;
	}
	public String toString(){
		return name;
	}
	public String getName(){
		return name;
	}
	public int getIncome(){
		return inc;
	}

	@Override
	public int compareTo(Person o) {
		if(this.getIncome() < o.getIncome()){
			return -1;
		}
		else if(this.getIncome() > o.getIncome()){
			return 1;
		}
		else{
			return 0;
		}
		
	}
}
	
	


