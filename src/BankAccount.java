
public class BankAccount implements Measurable {
	
	private double balance;
	private String name;
	public BankAccount(String name, double balance){
		this.name = name;
		this.balance = balance;
	}
	
	public double getMeasure() {
	return balance;
	}
	
	public String toString(){
		return name;
	}

	public double getBalance() {
		// TODO Auto-generated method stub
		return balance;
	}

}
