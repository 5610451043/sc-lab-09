
public class Country implements Measurable {
	
	private int area;
	private String name;
	
	public Country(String name, int area){
		this.name = name;
		this.area = area;
	}
	
	public double getMeasure() {		
		return area;
	}
	
	public String toString(){
		return name;
	}
	
	public int getArea() {
		// TODO Auto-generated method stub
		return area;
	}
	
	

}
